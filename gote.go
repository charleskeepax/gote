/* gote.go  --  gote Command Line Template Processor
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2013 Charles Keepax
 */
package main

import (
  "flag"
  "io/ioutil"
  "log"
  "os"
  "os/exec"
  "path/filepath"
  "regexp"
  "strings"
  "text/template"
)

var opts struct {
  outName, tmplName string
}

var tmpl *template.Template

func bash(arg string) string {
  cmd := exec.Command("bash", "-c", arg)
  result, err := cmd.Output()
  checkError(err)
  return string(result)
}

func gote(name string) string {
  text := new(memoryFile)

  if tmpl.Lookup(name) != nil {
    checkError(tmpl.ExecuteTemplate(text, name, tmpl))
  }

  return text.String()
}

func linesplit(input string) []string {
  return strings.Split(strings.Trim(input, "\n"), "\n")
}

func split(sep, input string) []string {
  return strings.Split(input, sep)
}

func streq(a, b string) bool {
  return a == b
}

func strcon(b, a string) bool {
  return strings.Contains(a, b)
}

func strcat(a, b string) string {
  return a + b
}

func regexreplace(regex, sub, source string) string {
  re := regexp.MustCompile(regex)
  return re.ReplaceAllString(source, sub)
}

var funcs template.FuncMap = template.FuncMap{
  "bash": bash,
  "gote": gote,
  "linesplit": linesplit,
  "split": split,
  "streq": streq,
  "strcon": strcon,
  "strcat": strcat,
  "regexreplace": regexreplace,
}

func checkError(err error) {
  if err != nil {
    log.Fatalln(err)
  }
}

func initialise() {
  flag.StringVar(&opts.outName, "o", "", "Output filename")
  flag.StringVar(&opts.tmplName, "t", "",
    "Template name to generate, default is first provided template file")
  flag.Parse()
}

func openOutputFile() *os.File {
  if opts.outName != "" {
    file, err := os.Create(opts.outName)
    checkError(err)
    return file
  }

  return os.Stdout
}

func closeOutputFile(file *os.File) {
  if file != os.Stdout {
    checkError(file.Close())
  }
}

func getTemplate() (*template.Template) {
  if flag.NArg() > 0 {
    if opts.tmplName == "" {
      _, opts.tmplName = filepath.Split(flag.Arg(0))
    }
    local, err := template.New("gotetmp").Funcs(funcs).ParseFiles(flag.Args()...)
    tmpl = local
    checkError(err)
    return tmpl
  } else {
    tmplData, err := ioutil.ReadAll(os.Stdin)
    checkError(err)
    opts.tmplName = "stdin"
    local, err := template.New(opts.tmplName).Funcs(funcs).Parse(string(tmplData))
    tmpl = local
    checkError(err)
    return tmpl
  }

  panic("Unreachable code!")
}

func main() {
  initialise()

  outFile := openOutputFile()
  defer closeOutputFile(outFile)

  checkError(getTemplate().ExecuteTemplate(outFile, opts.tmplName, nil))
}

